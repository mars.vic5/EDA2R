# Encriptacion
27/04/2017

Cesar

### Practica 26 (Cesar) - Martes
Realizar un programa que gestione a travez de delimitadores la información de la clase alumno, la cual cuenta con __codigo__, __nombre__ y __carrera__. El programa contara con el siguiente menú:
 1. Insertar
 2. Mostrar
 3. Encriptar (Imprimir)
 4. Desencriptar (Imprimir)

### Practica 27 (Huffman)
A travez del algoritmo de Huffman desarrollar un programa con el siguiente menu:
  1. Insertar frase
  2. Cragar e imprimir a memoria
  3. Imprimir lista
  4. Imprimir lista ordenada

### Practica 28
Implementar en la practica anterior:
  - Imprimir arbol

### Practica 29
Implementar la practica 26 usando XOR, el mismo menu y la llave sera `.`:

### Tarea
En el cuaderno hacer el arbol de huffman en el cuaderno

Usar encriptación César

## Huffman
Paso 1: Vaciar a memoria el archivo
``` JavaScript
let word = 'la ruta natural'
```

Paso 2: Crear TDA
Saco la primera letra del string `l`, busco sus repeticiones `2` veces y asi sucesivamente
```
 l, 2  ->
 a, 4  ->
 ' ', 2  ->
 r, 2  ->
 u, 2  ->
 t, 2  ->
 n, 1  -> FIN
```

Paso 3: Ordenar la lista.

Se ordena por 2 factores:
- Frecuencia
- ASCII
Mantener la lista ordenada

```
 n,   1   ->
 ' ', 2   ->
 l,   2   ->
 r,   2   ->
 t,   2   ->
 u,   2   ->
 a,   4   -> FIN
```

Paso 4: Crear un arbol

Todos los nodos podran ser raiz, los nuevos nodos se crean a con la fusión de dos nodos, siempre de la izquierda.
