# Dispersion

Ejemplo:

``` JavaScript
let Cancion = {
  name: '',
  genre: '',
  duration: ''
}

```

``` JavaScript
let mananitas = {
  name: 'Las mañanitas',
  genre: 'pop',
  duration: '3:04',
};
```
Archivo `canciones.txt`:

```
  _____________________________
0 |               |     |     |
1 |               |     |     |
2 |               |     |     |
3 |Las mañanitas  |pop  |3:04 |
4 |La cucaracha   |     |     |
5 |La vida loca   |     |     |
6 |Marcha funebre |     |     |
7 |Marcianito     |     |     |
8 |               |     |     |
  _____________________________
```

## Colisiones
A las llaves que por dispersión se convierten en la misma dirección se les denomina sinonimos, a las colosiones generan problemas debido a quee no se pueden colocar dos registros en el mismo espacio, para lo cual se podria utilizar algoritmos de dispersión que produzcan el menor numero posible de colisiones o hacer adecuaciones en la forma en la que se almacenan los registros.

```
Tarea:

Nombre      ASCII       Producto  Dirección Base
Antonio     
Marcelo     
Mariana
Monica
Maria
Julian
```

Multiplicar  el valor ASCII de los primeros dos caracteres y tomar los ultimos dos digitos del resultado para dirección.

## Reducir colisiones
Existen diferentes formas de reducir las coliciones:
1. Esparcir los registros: Esa solución requiere de la implementación de un algoritmo que distribuya los registros en forma aleatoria. Entre las direcciones disponibles. _Mientras más aleatorio mejor_
2. Usar memoria adicional: Es mpas facil encontrar un algoritmo que evite las coliciones si se tiene que distribuir en muchas direcciónes.
3. Colocar más de un registro en una sola dirección.

## Distribución de Poisson
Cuando se requiere predecir el numero posible de colisiones en un archivo que puede __guardar un solo registro por posición__. Primero se examina lo que le sucede a una dirección determinada cuando se aplica la función ed dispersión a una llave.

Cuando todas las llaves de una archivo se dispersan sería deseable poder contestar que probabilidad hay de que:
  1. Ninguno se disperse a la dirección dada.
  2. Exactamente dos, tres, cuatro, etc. se dispersen a una dirección.
  3. Exactamente una llave se disperse a la dirección.
  4. Todas las llaves del archivo se dispersan a la misma dirección.

```
p(x) = (r/N)^x * e^(-r/N) / x!
```

- __N__ Direcciones disponibles

- __r__ Registros que se van a almacenar

- __x__ NUmeroi de registro asignados a una dirección dada.

Entonces `p(x)` indica la probabilidad de que a una dirección determinada se hayan asginado __x__ registros despues de haber aplicado la función de dispersión a los __N__ registros.

### Ejerciciós
Supongase que hay 100 diracciones  y 100 registros cuyas llaves se dispersaran a las direcciónes.
  1. ¿Cual es la probabilidad de que una dirección dada no tenga llaves dispersadas a ella?
  2. ¿Cual es la probabilidad de que una direccción dada tenga exactamente una llave dispersada?
  3. ¿Cual es la probabilidad de que una dirección dada tenga dos llaves dispersadas?

## Densidad de Empaquetado
Este termino se refiere a la proporcion entre el numero de registro por almacenar y el numero de espacios disponibles.

__r__ = numero de registro
__N__ = numero de espacios

La densidad de empaquetamiento es el unico valor necesario para evaluar el desempeño en un ambiente de dispersión, suponinendo que el metodo de dispersión usado logra una distribución de registros razonablmenete aleatorios. No import ale tamaño real del archivo, xxxxxx. LO importante son los tamaños relativos dados por la densidad de empaquetamiento.

# Tarea
1. Utilizando las siguientes llaves: Monica, Olivia, Felipe, Sandra, Astrid, Angeles y Francisco; La función de disppersión sera la multiplicación de los caracteres de las cadenas de sus posiciones pares. Al numero resultante aplicar modulo 100.
  ``` Python
  words = ["oia" , "lva" , "eie" , "ada" , "srd" , "nee" , "rnic"]
  for word in words:
    ttl = 1
    print("Palabra: " + word)
    for c in word:
      ttl = ttl * ord(c)
    print("Producto: " + str(ttl))
    print("Module: " + str(ttl % 100) + "\n")
  ```

2. Supongase que 2000 direcciones y 1500 registros cuyas llaves se dispersan a las direcciones. Calcular la probabilidad de que:
  ``` Python
  def poisson(N, r, x):
    return (((r/N)**x) * math.exp(-(r/N)))/math.factorial(x)
  print(str( poisson(2000, 1500, 0)))
  print(str( poisson(2000, 1500, 2)))
  print(str( poisson(2000, 1500, 4)))
  ```
  1. Una dirección dada no tenga llaves dispersadas.
  2. Una dirección dad tenga dos llaves dispersadas.
  3. Una dirección dada tenga cuatro lalves dispersadas.
3. De acuerdo a la siguiente imagen determinar la densidad de empaquetamiento.
  ```
  _________________________
  |Guadalajara  |x|x|x| | |
  |Tlaquepaque  |x|x| | | |
  |Tonala       | | | | | |
  |Zapopan      |x|x|x|x|x|
  |_______________________|
  ```
