INVALID_IND = 0
EMPTY = 0

class FManager
  def initialize name, type
    @name = name          # Name
    @iname = 'i_' + name  # Indexes
    @type = type          # Template
    @ids = []
    @length = EMPTY
    @lastInd = INVALID_IND
    readIndexes
  end

  # Add object to file
  def add obj
    obj.id = @lastInd += 1
    reg = obj.toFile
    len = reg.length + 1

    # Data file
    File.open(@name, 'a') do |file|
      file.puts reg
    end

    # Id file
    File.open @iname, 'a' do |file|
      file.puts obj.id.chr + @length.to_s
    end

    @ids.push(Hash['id': obj.id, 'pos': @length]);
    @length += len
    puts @ids.to_s
  end

  def all
    begin
      file = File.new @name, 'r'
      objs = []
      while (line = file.gets)
        obj = @type.new
        obj.fromFile line
        objs.push obj
      end
      file.close # Cierra el archivo
      return objs
    rescue => err # Exception
      puts "[Exception] Error reading file on all function. #{err}"
      return []
    end
  end

  def find obj
    begin
      file = File.open @name, 'r'
      reg = @type.new
      while (line = file.gets)
        reg.fromFile line
        if obj.equals reg
          return reg
        end
      end
      return nil
    rescue => err
      puts "Ha ocurrido un error al buscar su registro. Error: #{err}"
      return nil
    end
  end

  def getIndexes
    return @ids
  end

  # Get index hash
  def readIndexes
    begin
      f = File.open @iname, "r"
      str = f.read
      ids = []
      lastInd = 0

      str.each_line do |line|
        ind = line[0].ord # Get id in position 0, and parse to number value
        pos = Integer line[1 .. line.length]
        if ind > lastInd
          lastInd = ind
        end
        ids.push Hash["id" => ind, "pos" => pos]
      end

      @ids = ids
      @lastInd = lastInd

      f = File.open @name, "r"
      @length = f.read.length
    rescue Exception => msg
      puts "Can't open indexes file"
    end
  end
end
