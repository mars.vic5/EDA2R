class FManager
  def initialize name
    @name = name
  end

  def add obj
    file = File.new @name, 'a' # Append mode
    if file
      file.puts "\n" + obj.to_file
      puts "Successfully saved " + obj.to_s
    else
      puts "Cant open the file #{@name}"
    end
    file.close
  end

  def edit obj
    obj.startSearch
    regs = IO.readLines(@name).map do |line|
      if obj.is line
        return obj.to_file
      else
        return line
      end
    end
    File.open(@name, 'w') do |file|
      file.puts regs
    end
    return obj.isFinded
  end
end
