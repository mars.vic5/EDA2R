FILE_NAME = 'Student.txt'

class Student
  def initialize(*args)
    if (args.length == 3)
      @name = args[0]
      @lastname = args[1]
      @age = args[2]
    else
      @name = ''
      @lastname = ''
      @age = ''
    end
    @finded = false
  end

  def setName(name)
    @name = name
  end
  def setLastname(lastname)
    @lastname = lastname
  end
  def setAge(age)
    @age = age
  end

  def getName
    return @name
  end
  def getLastname
    return @lastname
  end
  def getAge
    return @age
  end

  def to_s
    return "{ 'nombre': '#{@name}'\n  'apellido': '#{@lastname}'\n  'age': #{@age} }"
  end

  def to_file
    return @name + ',' + @lastname + ',' + @age.to_s;
  end

  def from_file line
    cols.split ','
    if cols.length == 3
      @name = cols[0]
      @name = cols[1]
      @name = cols[2].to_i
      return true
    else
      return false;
    end
  end

  def is line
    cols.slpit ','
    return cols.length == 3 && @name == cols[0]
  end

  def startSearch
    @finded = false
  end

  def isFinded
    return finded
  end
end
