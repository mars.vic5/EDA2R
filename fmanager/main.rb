require_relative 'fmanager'
require_relative 'student'

# Menu options
ADD     = '1'
EDIT    = '2'
LIST    = '3'
SEARCH  = '4'
DELETE  = '5'
EXIT    = '0'

F_NAME  = "students.txt"

class Main
  def initialize()
    @fmanager = FManager.new F_NAME
  end

  #Application main menu
  def menu
    op = EXIT
    begin
      puts "  Administrador de estudiantes\n"+
          "1.- Añadir\n" +
          "2.- Modificar\n" +
          "3.- Listar\n" +
          "4.- Buscar\n" +
          "5.- Eliminar\n" +
          "0.- Salir.\n" +
          " Seleccione una opción "
      op = gets.chomp
      case op
      when ADD
        add
      when EDIT
        # edit block
      when LIST
        # LIST BLOCK
      when SEARCH
        # SEARCH BLOCK
      when DELETE
        # DELETE BLOCK
      when EXIT
        # EXIT BLOCK. THIS BLOCK IS EMPTY
      else
        puts "Opción no valida. Intentelo de nuevo. "
        gets.chomp
      end
    end until op == EXIT
  end

  def add
    student = Student.new
    puts "Añadir studiante"
    puts "Nombre: "
    student.setName gets.chomp
    puts "Apellido: "
    student.setLastname gets.chomp
    puts "Edad: "
    student.setAge gets.chomp.to_i
    puts @fmanager.add student
  end

  def edit
    puts " Editar studiante"
    puts "Escriba el nombre del estudiante que desea editar"
    name = gets.chomp
    student = @fmanager.search name
    if student
      puts student
    else
      puts "No se encontro el estudiante especificado."
      gets.chomp
    end
  end
  def list
  end
  def search
  end
  def remove
  end
end

main = Main.new
main.menu
