require_relative "./node.rb"

###
# Doubly linked list, by Victor Diaz - mars.vic5@gmail.com
#
# Reference merge sort: http://www.geeksforgeeks.org/merge-sort-for-doubly-linked-list/
###
class List
  def initialize
    @anchor = Node.new
    @anchor.setNext @anchor
    @anchor.setPrev @anchor
  end

  def add val
    node = Node.new
    node.setVal val

    last = getLast

    last.getNext.setPrev node
    node.setNext last.getNext
    node.setPrev last
    last.setNext node
  end

  def find val
    node = @anchor.getNext
    while node != @anchor
      if node.getVal == val
        return node
      end
    end
    return nil
  end

  # Merge
  def merge first, second
    # if first list empty return second
    if first == @anchor
      return second
    end
    # if second list empty return first
    if second == @anchor
      return first
    end
    # Toma el valor más pequeño
    if first.getVal < second.getVal
      first.setNext merge(first.getNext, second)
      first.getNext.setPrev first
      first.setPrev @anchor
      return first
    else
      second.setNext merge(first, second.getNext)
      second.getNext.setPrev second
      second.setPrev @anchor
      return second
    end
  end

  # Merge sort
  def m_sort t_head
    if t_head == @anchor || t_head.getNext == @anchor
      return t_head
    end

    second = split t_head # Break the list in two pieces
    # Recursive
    t_head = m_sort t_head
    second = m_sort second

    # Merge the two sorted halves
    return merge t_head, second
  end

  # Break the list in two pieces
  def split t_head
    slow = t_head
    fast = t_head

    while fast.getNext != @anchor && fast.getNext.getNext != @anchor
      fast = fast.getNext.getNext
      slow = slow.getNext
    end

    temp = slow.getNext
    slow.setNext @anchor
    return temp
  end

  def sort
    head = m_sort @anchor.getNext
    @anchor.setNext head
    head.setPrev @anchor
  end

  def print
    node = @anchor.getNext

    while node != @anchor
      puts node.getVal.to_s
      node = node.getNext
    end
  end

  def getFirst
    return @anchor.getNext
  end

  def getLast
    return @anchor.getPrev
  end
end

List.new
