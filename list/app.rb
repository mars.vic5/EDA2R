require_relative './list.rb'

myList = List.new

myList.add "a"
myList.add "f"
myList.add "e"
myList.add "d"

puts "Unsorted: "
myList.print

myList.sort
puts "Sorted: "
myList.print
gets
