require_relative "program"
require_relative "fmanager"


Gem.win_platform? ? (CLEAR = "cls") : (CLEAR = "clear") # CLEAR constants support

# Menu options
ADD = 1
SHOW = 2
SEARCH = 3
EXIT = 0

FILE_NAME = 'programs.txt'

class Menu
  def initialize
    @fmanager = FManager.new FILE_NAME, Program

    begin
      system(CLEAR)
      puts " Menu de \"programas\"\n" +
        ADD.to_s + "- Agregar\n" +
        SHOW.to_s + "- Mostrar\n" +
        SEARCH.to_s + "- Buscar\n" +
        EXIT.to_s + "- Salir\n" +
        "Escribe una opcion: "
      op = gets.chomp.to_i
      case op
      when ADD
        add()
      when SHOW
        show()
      when SEARCH
        search()
      when EXIT
        puts "Bye, bye!"
      else
        puts "Opción no valida"
      end
    end until op == EXIT
  end

  def add
    puts " Añadir \"programa\"\n"
    program = Program.new

    puts "Nombre: "
    program.setName gets.chomp
    puts "Nivel:"
    program.setLevel gets.chomp
    puts "Vigencia: "
    program.setVigency gets.chomp
    puts "Duracion: "
    program.setDuration gets.chomp

    @fmanager.add program
  end

  def show
    puts " Lista de \"Programas\""
    regs = @fmanager.all
    regs.each { |prog| puts prog}
    puts "Presiona \"Enter\" para continuar."
    gets
  end

  def search
    prog = Program.new
    puts " Busqueda de \"Programa\""
    puts "Nombre de programa: "
    prog.setName gets.chomp
    reg = @fmanager.find prog
    if reg
      puts reg.to_s
    else
      puts 'No encontrado.'
    end
  end
end

Menu.new
