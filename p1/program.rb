DELIMITER = '||'
N_RGS = 4 # Number of register

# Register indexes
NAME_I = 0
LVL_I = 1
VGNC_I = 2
DUR_I = 3
# End block

class Program
  def initialize
    @name = ''
    @level = ''
    @vigency = ''
    @duration = ''
  end

  def setName name
    @name = name
  end

  def setLevel level
    @level = level
  end

  def setVigency vigency
    @vigency = vigency
  end

  def setDuration duration
    @duration = duration
  end

  def getName
    return @name
  end

  def getLevel
    return @level
  end

  def getVigency
    return @vigency
  end

  def getDuration
    return @duration
  end

  def to_s
    return "Nombre: #{@name}, Nivel: #{@level}, Vigencia: #{@vigency}, Duration: #{@duration}"
  end

  def toFile
    return @name + DELIMITER + @level + DELIMITER + @vigency + DELIMITER + @duration
  end

  def fromFile register
    regs = register.split DELIMITER
    if regs.length != N_RGS
      @name     = ''
      @level    = ''
      @vigency  = ''
      @duration = ''
      return false
    end
    @name = regs[NAME_I]
    @level = regs[LVL_I]
    @vigency = regs[VGNC_I]
    @duration = regs[DUR_I]
    return true
  end

  def equals program
    return program.getName == @name
  end
end
