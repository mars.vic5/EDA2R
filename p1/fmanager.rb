class FManager
  def initialize name, type
    @name = name
    @type = type
  end

  def add obj
    reg = obj.toFile
    File.open(@name, 'a') do |file|
      file.puts reg
    end
  end

  def all
    begin
      file = File.new @name, 'r'
      objs = []
      while (line = file.gets)
        obj = @type.new
        obj.fromFile line
        objs.push obj
      end
      file.close # Cierra el archivo
      return objs
    rescue => err # Exception
      puts "[Exception] Error reading file on all function. #{err}"
      return []
    end
  end

  def find obj
    begin
      file = File.new @name, 'r'
      reg = @type.new
      while (line = file.gets)
        reg.fromFile line
        if obj.equals reg
          return reg
        end
      end
      return nil
    rescue => err
      puts "Ha ocurrido un error al buscar su registro. Error: #{err}"
      return nil
    end
  end

end
