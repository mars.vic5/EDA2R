class Node
  def initialize
    @val = nil
    @next = nil
    @prev = nil
  end

  def setVal val
    @val = val
  end

  def setNext nxt
    @next = nxt
  end

  def setPrev prev
    @prev = prev
  end

  def getVal
    return @val
  end

  def getNext
    return @next
  end

  def getPrev
    return @prev
  end
end
