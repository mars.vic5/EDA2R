require_relative "list.rb"

INVALID_IND = 0
EMPTY = 0

class FManager
  def initialize name, type
    @name = name          # Name
    @iname = 'i_' + name  # Indexes
    @type = type          # Template
    @indexes = []
    @length = EMPTY
    @lastInd = INVALID_IND
    aux = @type.new
    @reg_len = aux.toFile.length
    readIndexes
  end

  # Add object to file
  def add obj
    reg = obj.toFile
    len = reg.length + 1

    # Data file
    File.open @name, 'a' do |file|
      file.puts reg
    end

    # Id file
    File.open @iname, 'a' do |file|
      file.puts obj.format_name + @length.to_s
    end

    @indexes.push(Hash['name': obj.name, 'pos': @length]);
    @length += len
    puts @ids.to_s
  end

  def edit o_reg, n_reg
    str = '' # str definition inside function scope
    File.open @name, 'r' do |file|
      str = file.read
      i_begin = indexOf o_reg.name
      data = n_reg.toFile
      i_end = i_begin + data.length - 1
      str[i_begin .. i_end] = data
      puts "from #{i_begin} to #{i_end}"
    end
    puts str
    File.open @name, 'w' do |file|
      file.write str
    end
    puts "Register edited"
    change_index_name o_reg.name, n_reg.name
    saveIndexes
  end

  def del obj
    name = obj.name
    len = @reg_len
    s_pos = 0
    @indexes.each do |i|
      if i["name"] == name
        s_pos = i["pos"]
        break
      end
    end
    e_pos = s_pos + len
    str = ''
    File.open @name, 'r' do |file|
      str = file.read
    end
    str[s_pos .. e_pos] = ''
    File.open @name, 'w' do |file|
      file.write str
    end

    rem_ind name
    saveIndexes
  end

  def all
    begin
      file = File.new @name, 'r'
      objs = []
      while (line = file.gets)
        obj = @type.new
        obj.fromFile line
        objs.push obj
      end
      file.close # Cierra el archivo
      return objs
    rescue => err # Exception
      puts "[Exception] Error reading file on all function. #{err}"
      return []
    end
  end

  def find obj
    begin
      file = File.open @name, 'r'
      reg = @type.new
      while (line = file.gets)
        reg.fromFile line
        if obj.equals reg
          return reg
        end
      end
      return nil
    rescue => err
      puts "Ha ocurrido un error al buscar su registro. Error: #{err}"
      return nil
    end
  end

  def indexOf name
    res = @indexes.find { |el| el["name"] == name }
    return res["pos"]
  end

  def file_length
    return @length
  end

  def getIndexes
    return @indexes
  end

  # Get index hash
  def readIndexes
    puts "Loading indexes"
    begin
      f = File.open @iname, "r"
      str = f.read
      indexes = []
      obj = @type.new

      str.each_line do |line|
        name = line [0 .. @type.STR_LENGTH - 1].strip
        pos = Integer line[@type.STR_LENGTH .. line.length]
        indexes.push Hash["name" => name, "pos" => pos]
        puts "Loading index #{name}"
      end
      @indexes = indexes

      f = File.open @name, "r"
      @length = f.read.length
    rescue Exception => msg
      puts "Can't open indexes file + #{msg}"
    end
    gets
  end

  def saveIndexes
    puts "Writting indexes"
    obj = @type.new
    regs = ''
    @indexes.each do |ind|
      obj.name = ind["name"]
      puts obj.name
      regs << obj.format_name
      regs << ind["pos"].to_s
      regs << "\n"
    end
    file = File.open @iname, 'w' do |file|
      file.write regs
    end
    puts "Indexes saved"
    puts regs
  end

  def rem_ind name
    len = @reg_len
    n_ind = [] # New indexes arr
    back = 0
    @indexes.each do |ind|
      if ind["name"] != name
        ind["pos"] -= back
        n_ind.push ind
      else
        back += len + 1 # Register length + end line
      end
    end
    @indexes = n_ind
  end

  def change_index_name o_name, n_name
    @indexes.map do |el|
      if el["name"] == o_name
        el["name"] = n_name
      end
      el
    end
  end

  def sort
    my_list = List.new
    @indexes.each do |el|
      my_list.add el
    end
    puts "List"
    my_list.sort
    my_list.print
  end
end
