require_relative "patient"
require_relative "fmanager"


Gem.win_platform? ? (CLEAR = "cls") : (CLEAR = "clear") # CLEAR constants support

# Menu options
ADD = 1
SHOW = 2
SEARCH = 3
EDIT = 4
DEL = 5
SHOW_I = 6
SHOW_IO = 7
EXIT = 0

FILE_NAME = 'patients.txt'

class Menu
  def initialize
    @fmanager = FManager.new FILE_NAME, Patient

    begin
      system(CLEAR)
      puts " Menu de \"pacientes\"\n" +
        ADD.to_s + "- Agregar\n" +
        SHOW.to_s + "- Mostrar\n" +
        SEARCH.to_s + "- Buscar\n" +
        EDIT.to_s + "- Modificar registro\n" +
        DEL.to_s + "- Eliminar registro\n" +
        SHOW_I.to_s + "- Mostrar indices\n" +
        SHOW_IO.to_s + "- Mostrar indices ordenados\n" +
        EXIT.to_s + "- Salir\n" +
        "Escribe una opcion: "
      op = gets.chomp.to_i
      case op
      when ADD
        add()
      when SHOW
        show()
      when SEARCH
        search()
      when SHOW_I
        showIndexes()
      when SHOW_IO
        show_ind_ord()
      when EDIT
        edit()
      when DEL
        del()
      when EXIT
        puts "Bye, bye!"
      else
        puts "Opción no valida"
      end
    end until op == EXIT
  end

  def add
    puts " Añadir \"paciente\"\n"
    patient = Patient.new

    puts "Nombre: "
    patient.name = gets.chomp
    puts "Padecimietno: "
    patient.sickness = gets.chomp
    puts "Tratamiento: "
    patient.treatment = gets.chomp

    if @fmanager.find patient
      puts "Ya existe un paciente con este nombre."
    else
      @fmanager.add patient
    end
    gets
  end

  def show
    puts " Lista de \"Paciente\""
    regs = @fmanager.all
    regs.each { |prog| puts prog}
    puts "Presiona \"Enter\" para continuar."
    gets
  end

  def search
    prog = Patient.new
    puts " Busqueda de \"Paciente\""
    puts "Nombre de programa: "
    prog.name = gets.chomp
    reg = @fmanager.find prog
    if reg
      puts reg.to_s
    else
      puts 'No encontrado.'
    end
    gets
  end

  def edit
    patient = Patient.new
    puts "Editar"
    puts "Nombre"
    patient.name = gets.chomp
    res = @fmanager.find patient
    if res
      puts "Nombre [#{patient.name}]"
      res.name = gets.chomp
      puts "Padecimiento [#{patient.sickness}]"
      res.sickness = gets.chomp
      puts "Tratamiento [#{patient.treatment}]"
      res.treatment = gets.chomp

      if (res.name != patient.name) && @fmanager.find(res)
        puts "Ya existe un registro con ese nombre"
        return
      end
      puts res.to_s
      @fmanager.edit patient, res
    else
      puts "Registro no encontrado"
    end
    gets
  end

  def del
    patient =  Patient.new
    puts "Eliminar registro"
    puts "Nombre: "
    patient.name = gets.chomp
    patient = @fmanager.find patient
    if patient
      @fmanager.del patient
      puts "Paciente eliminado"
    else
      puts "Registro no encontrado"
    end
    gets
  end

  def showIndexes
    puts "  Indices:"
    puts "Largo " + @fmanager.file_length.to_s
    puts @fmanager.getIndexes.to_s
    gets
  end

  def show_ind_ord
    @fmanager.sort
    gets
  end
end

Menu.new
