DELIMITER = '|'
N_RGS = 3

STR_LENGTH = 25
STR_TEMPLATE = "%#{STR_LENGTH}.#{STR_LENGTH}s"

class Patient
  attr_accessor :id, :name, :sickness, :treatment

  # constants
  def self.STR_LENGTH
    return STR_LENGTH
  end

  def format_name
    return STR_TEMPLATE % @name
  end

  def toFile
    return (STR_TEMPLATE % @name) +
        (STR_TEMPLATE % @sickness) +
        (STR_TEMPLATE % @treatment)
  end

  def fromFile line
    s = 0
    e = s + STR_LENGTH - 1
    @name = line[s .. e].strip
    s = e + 1
    e = s + STR_LENGTH - 1
    @sickness = line[s .. e].strip
    s = e + 1
    e = s + STR_LENGTH - 1
    @treatment = line[s .. e].strip
  end

  def to_s
    return "Nombre: #{@name}, padecimiento: #{@sickness}, tratamiento: #{@treatment}"
  end

  def equals patient
    return patient.name == @name
  end
end
